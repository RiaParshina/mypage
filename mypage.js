
if (Meteor.isClient) {
  // counter starts at 0
  //Session.setDefault('counter', 0);
  
  Meteor.startup(function () {
  particlesJS('particles-js', {
  	particles: {
    color: '#fff',
    color_random: false,
    shape: 'circle', // "circle", "edge" or "triangle"
    opacity: {
      opacity: 1,
      anim: {
        enable: true,
        speed: 1.5,
        opacity_min: 0,
        sync: false
      }
    },
    size: 4,
    size_random: true,
    nb: 150,
    line_linked: {
      enable_auto: true,
      distance: 100,
      color: '#fff',
      opacity: 1,
      width: 1,
      condensed_mode: {
        enable: false,
        rotateX: 600,
        rotateY: 600
      }
    },
    anim: {
      enable: true,
      speed: 1
    }
  },
  interactivity: {
    enable: true,
    mouse: {
      distance: 300
    },
    detect_on: 'canvas', // "canvas" or "window"
    mode: 'grab', // "grab" of false
    line_linked: {
      opacity: .5
    },
    events: {
      onclick: {
        enable: true,
        mode: 'push', // "push" or "remove"
        nb: 4
      },
      onresize: {
        enable: true,
        mode: 'out', // "out" or "bounce"
        density_auto: false,
        density_area: 800 // nb_particles = particles.nb * (canvas width *  canvas height / 1000) / density_area
      }
    }
  },
  /* Retina Display Support */
  retina_detect: true
});
      });
 

  Template.navpanel.helpers({
    
  });

  Template.hello.helpers({
    
  });

   Template.experience.helpers({
    
  });
  
   Template.contact.helpers({
    
  });

  Template.hello.events({
   
  });
  
//rendered 
 
  Template.hello.rendered = function() {
  	var leavedPage;
    $('.not-visible').removeClass('not-visible').addClass('visible');

    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var numberOfParticles = .00005 * (windowWidth * windowHeight);
  
    $('#fullpage').fullpage({
    anchors: ['hello', 'experience', 'contact'],
    /*sectionsColor: ['green', '#4BBFC3', '#7BAABE'],  //colours of the pages. To change*/
    menu: '#nav-menu',
    scrollOverflow: true,
    easing: 'easeInExpo',
    css3: false,
    scrollingSpeed: 555,
    navigation: true,
    afterLoad: function (anchorLink) {
    	//console.log(leavedPage);
        if (leavedPage != undefined) {
            leavedPage.find('.animated').removeClass('viewable');
        }
            leavedPage = $('#' + anchorLink + '-page');
            leavedPage.find('.animated').addClass('viewable');
        },
    afterRender: function () {
        scrollToAnchor();
        }           
            
  });   
} 

  
} //end of meteor client



if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}

function scrollToAnchor() {
            //getting the anchor link in the URL and deleting the `#`
            var value = window.location.hash.replace('#', '').split('/');
            var section = value[0];
			//console.log(value);
			//console.log(section);
            if (section) {  //if theres any #
                $.fn.fullpage.silentMoveTo(section);
            }
        }
        
